#! /usr/bin/env python
import rospy
from std_msgs.msg import String

class Reception(object):
    def __init__(self):
        self.msg1 = []
        self.msg2 = []
        self.msg3 = []
        rospy.init_node('getinfo', anonymous=True)
        self.pub_ = rospy.Publisher("pass_info", String, queue_size=10)
        rospy.Subscriber("lm_data", String, self.callback)
        rospy.spin()

    def callback(self, detected_info):
        rospy.loginfo(detected_info.data)
        if detected_info.data.find("LISA") > -1:
            self.msg1 = "Her name is Lisa"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("ANDY") > -1:
            self.msg1 = "His name is Andy"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("COKE") > -1:
            self.msg2 = "His or Her favourite drink is coke"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("JUICE") > -1:
            self.msg2 = "His or Her favourite drink is juice"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("WINDOW") > -1:
            self.msg3 = "We need a seat near the window"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("ALONE") > -1:
            self.msg3 = "We need a seat that is alone"
            #rospy.loginfo(detected_info.data)
        elif detected_info.data.find("THAT'S ALL") > -1:
            self.pub_.publish(self.msg1)
            rospy.sleep(3.)
            self.pub_.publish(self.msg2)
            rospy.sleep(3.)
            self.pub_.publish(self.msg3)
    def shutdown(self):
        rospy.loginfo("Stop Reception")
        rospy.sleep(1)


if __name__ == '__main__':
    Reception()
